import uuid
import threading as thr
import time
import math
from random import randint

nodeCounter = 0
lockCount = 4
nodes = []
clients = {}
clientAlphabet = 'AB'
WINDOW = 3


# TODO: Finish paxos up until decide on a command 
# TODO: implement log for each node, and on exit output log to a text file 
# TODO: maybe stuff nodes into each node rather than relying on global thing
# TODO: clients randomly generate commands
# TODO: packet loss


def init_nodes(nodeCount):
	global nodes
	for i in range(nodeCount):
		nodes.append(nodeThread(i, nodes))
		nodes[i].start()

def init_clients():
	global clientAlphabet
	global clients
	for i, c in enumerate(clientAlphabet):
		clients[c] = clientThread(c)
		clients[c].start()

def init_locks():
	global lockCount
	return [Lock(i) for i in range(lockCount)]

class nodeThread(thr.Thread):

	def __init__(self, nodeID, nodes):

		thr.Thread.__init__(self)

		# Define the initial state of the node locks (replicated state machine)
		self.locks = init_locks()
		self.slot_in = 1
		self.slot_out = 1 
		self.requests = []
		# keys of dicts are the proposal numbers, the value is the command 
		self.proposals = {}
		self.decisions = {} 
		self.nodes = nodes
		self.proposal_counter = 0
		self.accept_proposal = None

		self.accepts_received = {}
		self.majority = math.floor(len(nodes)/2)


		# Might not need, but good for debugging
		self.nodeID = nodeID

		# set some flags for whether the node is dead, partitioned, etc.
		self.dead = False
		self.partitioned = False

		# Define some events, might not need
		self._please_die = thr.Event()
		self._has_proposal = thr.Event()
		self._has_decision = thr.Event()
		self._has_majority_promise = thr.Event()


		### Define initial state of an acceptor
		self.promised_proposal_numbers = {}
		self.accepted_proposal_numbers = {}

		self._recieved_prepare_request = thr.Event()
		self.recieved_prepare_request_storage = None
		self._recieved_accept_request = thr.Event()


		# print('starting node ', self.nodeID, ' with locks ', self.locks)


	def run(self):
		while not self._please_die.is_set():
			#print("i'm alive")
			
			if self._has_proposal.is_set():
				self.proposer_propose()
				self._has_proposal.clear()

			if self._has_majority_promise.is_set():
				print('Node ', self.nodeID, ' got a majority and is sending an accept request for proposal ', self.accept_proposal)
				self.proposer_send_accept_request(self.slot_in)
				self._has_majority_promise.clear()

			if self._has_decision.is_set():
				# perform all decisions# TODO: check if this will block for i...
				for key in self.decisions.keys():
					command = self.decisions[key]
					if key in self.proposals.keys():
						proposal = self.proposals.pop(s)
						if command != proposal:
							self.requests += proposal
					self.proposer_perform(command)
					self._has_decision.clear()

			# acceptor stuff
			if self._recieved_prepare_request.is_set():
				self.acceptor_receive_prepare_request()
				self._recieved_prepare_request.clear()



			time.sleep(1) #sleep 1 second




	# This is just a way for the run function to call another function
	def acceptor_set_received_prepare_request_flag(self, proposal, slot_number):
		self._recieved_prepare_request.set()
		self.recieved_prepare_request_storage = [proposal, slot_number]


	# For now we are circumventing the above function so that a proposer 
	def acceptor_receive_prepare_request(self, proposal, slot_number):

		self.recieved_prepare_request_storage = None

		proposalID = proposal[0] # <NodeID, propID>

		# Gets node's existing promised and accepted proposas
		if slot_number in self.promised_proposal_numbers.keys():
			promised_proposal = self.promised_proposal_numbers[slot_number]
		else:
			promised_proposal = None

		if slot_number in self.accepted_proposal_numbers.keys():
			accept_proposal = self.accepted_proposal_numbers[slot_number]
		else:
			accept_proposal = None


		# checks if node has an existing accepted proposal
		if accept_proposal:
			print('Node ', self.nodeID, ' has already accepted proposal' , accept_proposal)
			return (False, accept_proposal, 'acc')

		# accept_proposal = self.accepted_proposal_numbers[slot_number]

		# 1 - No promised  id
		# set promised id to id  of that thing, return that 'promise' 
		if not promised_proposal:
			self.promised_proposal_numbers[slot_number] = proposal
			print('Node ', self.nodeID, ' has now promised proposal' , proposal)
			return (True, proposal, 'prom')

		# 2 - proposal ID > promised ID
		# Ideally would check if the value of the proposal is the same, if it is then increment the promised ID
		# But since we can't have two of the Return 'no'
		if promised_proposal[0][1] < proposalID[1] or ((promised_proposal[0][1] == proposalID[1]) and (promised_proposal[0][0] < proposalID[0])):
			self.promised_proposal_numbers[slot_number] = proposal
			print('Node ', self.nodeID, ' has now promised proposal' , proposal)
			return (True, proposal, 'prom')

		else:
			print('Node ', self.nodeID, ' has already promised proposal' , promised_proposal)
			return (False, promised_proposal, 'prom')





	# function for node to propose a new command
	# TODO: possibly add in + WINDOW for slot_in < slot_out check, this is for reconfiguration
	def proposer_propose(self):
		# checking requests is a python-y way of checking if requests is empty
		while self.slot_in < self.slot_out + WINDOW and self.requests: 
			# TODO: might need to check a reconfigure here # 
			# Check if there exists anything already in the decision log with the same key (slot_in)
			if self.slot_in not in self.decisions.keys():
				# get a new request 
				request = self.requests.pop(0) # note that this takes the first request from the list
				print(request)
				# add this request into the proposal dict
				self.proposals[self.slot_in] = request
				# send the proposal to all the other nodes!
				self.proposer_send_prepare_request(request, self.slot_in)


	def proposer_send_prepare_request(self, request, slot_number):

		global nodes

		majority = math.floor(len(nodes)/2)
		collected_responses = 0
		highest_proposal_number = self.proposal_counter

		# make proposal #
		proposalID = (self.nodeID, self.proposal_counter)
		proposal = (proposalID, request)

		print('node ', self.nodeID, ' sending prepare request ', proposal, 'in slot ', slot_number)

		for n in nodes:
			reply = n.acceptor_receive_prepare_request(proposal, slot_number)
			# reply is of form (bool, proposal, 'prom'|'acc')
			if reply[0]:
				collected_responses += 1
			else:
				higher_proposal = reply[1]
				if highest_proposal_number <= higher_proposal[0][1]:
					highest_proposal_number = higher_proposal[0][1]
				if reply[2] == 'acc':
					
					# TODO:  check an accept and send that instead
					# If an accept, then package that and do it, and push your own request back down
					# If a promise only then just repackage your own as a higher number

		if highest_proposal_number != self.proposal_counter:
			self.requests.append(request)
			self.proposal_counter = highest_proposal_number
			self._has_proposal.set()
			print('node ', self.nodeID, ' is increasing it\'s proposal counter')
		elif collected_responses > majority:
			self._has_majority_promise.set()
			self.accept_proposal = proposal
			print('node ', self.nodeID, ' got a majority')
		else:
			global clients
			clients[proposal[1][0]]._request_failed.set()

		self.proposal_counter += 1


	def proposer_send_accept_request(self, slot_number):
		print('node ', self.nodeID, ' is beginning asking for accept requests')

		global nodes
		highest_proposal_number = self.proposal_counter

		majority = math.floor(len(nodes)/2)
		collected_responses = 0
		proposal = self.accept_proposal
		self.accept_proposal = None

		print('node ', self.nodeID, ' sending accept request ', proposal, 'in slot #', slot_number)

		for n in nodes:
			reply = n.acceptor_receive_accept_request(proposal, slot_number)
			# reply is of form (bool, proposal, 'prom'|'acc')
			if reply[0]:
				collected_responses += 1
			else:
				higher_proposal = reply[1]
				if highest_proposal_number <= higher_proposal[0][1]:
					highest_proposal_number = higher_proposal[0][1]

		if highest_proposal_number != self.proposal_counter:
			self.requests.append(request)
			self.proposal_counter = highest_proposal_number
			self._has_proposal.set()
		elif collected_responses > majority:
			self._has_majority_promise.set()
			self.accept_proposal = proposal
		else:
			global clients
			clients[proposal[1][0]]._request_failed.set()

		self.proposal_counter += 1
		print('node ', self.nodeID, ' is done with asking for accept requests')

	def acceptor_receive_accept_request(self, proposal, slot_number):

		print('Node ', self.nodeID, ' recieved a accept request for ' ,
			proposal, ' in slot # ', slot_number)

		proposalID = proposal[0] # <NodeID, propID>

		#Gets node's existing promised and accepted proposas
		if slot_number in self.promised_proposal_numbers.keys():
			promised_proposal = self.promised_proposal_numbers[slot_number]
		else:
			promised_proposal = None

		if slot_number in self.accepted_proposal_numbers.keys():
			accept_proposal = self.accepted_proposal_numbers[slot_number]
		else:
			accept_proposal = None

		if not promised_proposal:
			self.promised_proposal_numbers[slot_number] = proposal
			self.accepted_proposal_numbers[slot_number] = proposal
			print('Node ', self.nodeID, ' has now accepted proposal' , proposal, ' for slot ', slot_number)
			return (True, proposal, 'prom')

		# 2 - proposal ID > promised ID
		# Ideally would check if the value of the proposal is the same, if it is then increment the promised ID
		# But since we can't have two of the Return 'no'
		if promised_proposal[0][1] <= proposalID[1] or ((promised_proposal[0][1] == proposalID[1]) and (promised_proposal[0][0] <= proposalID[0])):
			self.promised_proposal_numbers[slot_number] = proposal
			self.accepted_proposal_numbers[slot_number] = proposal

			print('Node ', self.nodeID, ' has now accepted proposal' , proposal, ' for slot ', slot_number)
			self.acceptor_send_accepted(proposal, slot_number)
			return (True, proposal, 'acc')

		else:
			print('Node ', self.nodeID, ' did NOT accepted proposal' , proposal, ' for slot ', slot_number, ' becasue it had promised a proposal ', promised_proposal)
			return (False, promised_proposal, 'prom')

	def acceptor_send_accepted(self, proposal, slot_number):
		global nodes

		for n in nodes:
			n.learner_receive_accept(proposal, slot_number)

	def learner_receive_accept(self, proposal, slot_number):
				
		if slot_number in self.accepts_received.keys():
			if proposal in self.accepts_received[slot_number].keys():
				self.accepts_received[slot_number][proposal] += 1
			else:
				self.accepts_received[slot_number][proposal] = 1
		else:
			self.accepts_received[slot_number] = {}
			self.accepts_received[slot_number][proposal] = 1

		if self.accepts_received[slot_number][proposal] > self.majority:
			print('node ', self.nodeID, ' is sending a decide for ', proposal, ' at slot ', slot_number)

			self.decide(proposal, slot_number)

			# send decision to everyone else
			global nodes
			for n in nodes:
				n.decide(proposal, slot_number)

	def decide(self, proposal, slot_number):
		self.decisions[slot_number] = proposal[1]

		while self.slot_out in self.decisions.keys():
			print('node ', self.nodeID, ' is about to preform decision ', self.decisions[self.slot_out], ' at slot number ', self.slot_out)
			self.perform(self.decisions[self.slot_out])
			self.slot_out += 1

		self.slot_in = self.slot_out
		print('node ', self.nodeID, ' finished decision and set slot_in to slot_out (the next command it needs)')
		

	#Lock command is <clientID, lockID, toLock>

	# value is the command to run, which should contain the client info as well 
	# this actually executes the command
	def perform(self, lockCommand):
		print('node ', self.nodeID, ' preforming command ', lockCommand)

		if lockCommand[2]:
			#if True, then the command is lock
			success = self.locks[lockCommand[1]].getLock(lockCommand[0])
			print('client ', lockCommand[0], ' has locked lock# ', self.locks[lockCommand[1]].lockID, self.locks[lockCommand[1]].isLocked  )

		else: #command is unlock
			success = self.locks[lockCommand[1]].unlock()

		print(success)
		# self.slot_in += 1
		return success


	def proposer_recieve_request(self, value):
		self._has_proposal.set()
		self.requests.append(value)

		# # set the value, but don't pass it in the prepare proposal
		# self.value = value
		# self.proposal_number = (self.nodeID, self.proposalCounter)
		# self.proposalCounter += 1

	def proposer_recieve_decision(self, key, value):
		self._has_decision.set()

		# set the value, but don't pass it in the prepare proposal
		self.decisions[key] = value

		# self.proposal_number = (self.nodeID, self.proposalCounter)
		# self.proposalCounter += 1


	def get_ID(self):
		return self.nodeID;



	#event setting
	def kill(self):
		self._please_die.set()

class Lock:
	def __init__(self, id):
		self.lockID = id
		self.isLocked = False
		self.lockHolder = None

	def getLock(self, clientID):
		#check if unlocked
		if not self.isLocked:
			self.lockHolder = clientID
			self.isLocked = True
			return True
		else:
			return False

	def unlock(self):
		#Anyone can unlock it, since its unspecified

		if self.isLocked:
			self.lockHolder = None
			self.isLocked = False
			
		return True


class clientThread(thr.Thread):

	def __init__(self, id):
		thr.Thread.__init__(self)
		self.clientID = id
		self._request_failed = thr.Event()

	def run(self):
		global nodes

		prop = ('A', 1, True)
		# prop2 = ('A', 2, True)
		# nodes[0].learner_receive_accept(prop, 1)
		# nodes[0].learner_receive_accept(prop2, 1)
		nodes[randint(0,len(nodes) - 1)].proposer_recieve_request(prop)

		# while True:
		# 	# print('Client ', self.clientID, 'is running.')
		# 	if self._request_failed.is_set():
		# 		print('Client ', self.clientID, ' failed request')
		# 		self._request_failed.clear()
		# 	time.sleep(5)