# import http.server
# import socketserver

# PORT = 8000

# Handler = http.server.SimpleHTTPRequestHandler

# with socketserver.TCPServer(("", PORT), Handler) as httpd:
#     print("serving at port", PORT)
#     httpd.serve_forever()

import socketserver
import time
from multiprocessing import Pool

def to_upper(data):
	return data.upper()

def 
	

class MyTCPHandler(socketserver.BaseRequestHandler):
	"""
	The request handler class for our server.

	It is instantiated once per connection to the server, and must
	override the handle() method to implement communication to the
	client.
	"""
	# Taken from: https://docs.python.org/3/library/socketserver.html
	def handle(self):
		# self.request is the TCP socket connected to the client
		self.data = self.request.recv(1024).strip()
		print("{} wrote:".format(self.client_address[0]))
		print(self.data)

		# Very simple multiprocessing example
		# make a pool of 5 processes
		pool = Pool(processes=5, init_node)

		# have one processes run the function (which just upper cases things) with the data
		result = pool.apply_async(to_upper, (self.data,))

		# prints "100" unless your computer is *very* slow
		print(result.get(timeout=1))
				
		# just send back the same data, but upper-cased
		self.request.sendall(result.get(timeout=1))

if __name__ == "__main__":
	HOST, PORT = "127.0.0.1", 9999

	# Create the server, binding to localhost on port 9999
	with socketserver.TCPServer((HOST, PORT), MyTCPHandler) as server:
		# Activate the server; this will keep running until you
		# interrupt the program with Ctrl-C
		server.serve_forever()



# from multiprocessing import Process, Queue

# def f(q):
#     q.put([42, None, 'hello'])

# if __name__ == '__main__':
#     q = Queue()
#     p = Process(target=f, args=(q,))
#     p.start()
#     print(q.get())    # prints "[42, None, 'hello']"
#     p.join()


