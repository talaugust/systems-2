Christine Geeng (cgeeng) 1723434
Tal August (taugust) 1723323

HOW TO USE
python3 run.py

(I'll probably create a way to kill a node on command)

IMPLEMENTATION

We implemented a leaderless paxos which mimics the server, multiple nodes, and multiple clients through threads controlling objects. We simulate a network by having each messages have a probability of not being sent (packet drop). Nodethreads at random will "die" and will not send or receive messages. Nodes can also be partitioned, which simulates a network partition, and not recieve or send messages during that time.

We use thread flags to handle messaging. When a client gives a request to a node, it saves the request in the node's request queue, and sets a flag in the node so it checks for a request to package into a proposal. We use a similar framework for other message passing. Many messages rely on node functions to pass a request back and forth between nodes. 

Each node simulates a replicated state machine by containing a log of all decided commands. Each node executes this commands in order, blocking on any missing command in the sequence (a node will propose a command for this spot to learn what command has already been decided and fill this spot in with the command it learns). The replicated state machine is a lock server, where each node holds a series of locks that clients can request to lock or unclock. 

We create unique proposal ids by using both a (local to node) proposal counter and the node id that created the proposal. If proposal ids are the same, then the higher nodeid will be considered the higher number proposal.


State Machine Stuff

ASSUMPTIONS

We assume no proposal will ever be the same (because it includes the client which asked for the command), so we can compare proposal numbers and assume they are totally ordered. We do not compare commands since each is unique (we assume a client cannot send the same command to two nodes at the same time).

ISSUES

We are unsure if the nodes will correctly propose a command if a command is missing in their log. This functionality is implemented to the best of our ability, but we could not test it adequately. 




a detailed description of your group's implementation
any assumptions you made
how to use your implementation
any outstanding issues
anything else that you feel is important to discuss (e.g. quirks, interesting behavior, performance characteristics, etc)

